#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Bhushan Mohan'
SITENAME = 'Bhushan Mohan'

SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public'

TIMEZONE = 'America/Los_Angeles'

DEFAULT_LANG = 'en'
# ABOUT_ME = 'HELLO'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
SOCIAL = (('Linkedin', 'https://www.linkedin.com/in/itzbhushan/'),
          ('Twitter', 'https://twitter.com/itzbhushan'),
          ('Stack-overflow', 'https://stackoverflow.com/users/1637197/bhushan'),
          ('Gitlab', 'https://gitlab.com/itzbhushan'),
          ('Google Scholar',
           'https://scholar.google.com/citations?user=InG_pqMAAAAJ'),
          )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True
THEME = 'pelican-themes/pelican-bootstrap3'
BOOTSTRAP_THEME = 'cosmo'
PLUGIN_PATHS = ['pelican-plugins']
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGINS = ['i18n_subsites']

I18N_TEMPLATES_LANG = 'en'
USE_FOLDER_AS_CATEGORY = True
STATIC_PATHS=['files']
